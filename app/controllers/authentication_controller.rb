class AuthenticationController < ApplicationController

  def login
    user=User.find_by(email: params[:user][:email])
    user=user&.authenticate(params[:user][:password])
    if user
      token=JsonWebToken.encode([user_id: user.id])
      render json: {token: token}
    else
      render json: {message: "Usuário ou senha incorretos"}, status:401
    end
  end

  def sign_up_user

    @user = User.new(user_params)

    if current_user != nil
      if current_user.role=="admin"#admin cria funcionario
        @user.role="clerk"
      elsif current_user.role=="clerk"#funcionario cria cliente
        @user.role="client"
      else #cliente nao cria nada
        render json:{message: "Permissao negada"},status: 403
        return nil
      end
    else
      render json:{message:"Faça o login para continuar"}
      return nil
    end

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :password,:password_confirmation, :email, :cpf, :telephone)
  end

end
