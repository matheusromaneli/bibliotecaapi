class LoansController < ApplicationController
  load_and_authorize_resource
  before_action :set_loan, only: [:show, :update, :destroy, :close_loan]

  # GET /loans
  def index
    @loans = Loan.all

    render json: @loans
  end

  # GET /loans/1
  def show
    render json: @loan
  end

  # POST /loans
  def create
    user= User.find(loan_params[:user_id])
    
    if state_type(user)
      @loan = Loan.new(loan_params)
      @loan.fine=0
      @loan.delay_days=0
      @loan.status="Aberto"
    else
      render json:{message: "O usuário está com empréstimo ativo"}
      return nil
    end

    if @loan.save
      render json: @loan, status: :created, location: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  def close_loan
    
    if late(@loan)
      fine_value(@loan)

    end

    @loan.status="Fechado"
    if @loan.save
      render json: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /loans/1
  def update
    if @loan.update(loan_params)
      render json: @loan
    else
      render json: @loan.errors, status: :unprocessable_entity
    end
  end

  # DELETE /loans/1
  def destroy
    if late(@loan)
      return nil
    else
      @loan.destroy
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan
      @loan = Loan.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def loan_params
      #elementos usados para a criaçao
      params.require(:loan).permit( :user_id, :book_id, :expiry_date)
    end

    def late(loan)
      puts Date.today
      loan.expiry_date < Date.today
    end

    def fine_value(loan)
      loan.delay_days = Date.today - loan.expiry_date
      loan.fine= loan.delay_days.to_f * 5.0
    end

    def state_type(user)
      if user.loans.present?
        if user.loans.last.status == "Fechado"
          return true
        end
      else
        return true
      end
    end
end
