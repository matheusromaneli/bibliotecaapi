class Book < ApplicationRecord
  belongs_to :author

  validates :name, :url_image, :edition, :theme, :issue, :kind, :author_id, presence: true
end
