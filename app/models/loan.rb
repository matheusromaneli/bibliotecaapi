class Loan < ApplicationRecord
  belongs_to :user
  belongs_to :book

  validates :user_id, :book_id, presence: true
  validates :expiry_date,presence: true, format:{with: /\A20[0-9][0-9]+-+[0-1][0-9]+-+[0-3][0-9]\Z/ ,message: "insira a data no formato: AAAA-MM-DD"}
  
end
