class User < ApplicationRecord
    has_secure_password
    has_many :loans

    validates :name, :password_confirmation, :password, presence: true
    validates :cpf, presence: true, uniqueness:true, format:{with: /\A([0-9]{3}+.){2}+[0-9]{3}+-+[0-9]{2}+\Z/ ,message:"Ex.: XXX.XXX.XXX-XX"}
    validates :email, presence: true, uniqueness:true,format:{with: /\b[A-Z0-9._%a-z\-]+@id.uff.br\Z/,message:"Ex.: example@id.uff.br"}
    validates :telephone, presence: true, uniqueness:true,format:{with: /\A[0-9]{4}+-+[0-9]{4}\Z/ ,message:"Ex.: 9999-9999"}

    #enum role:{"client":0,"clerk":1,"admin":2}
    
end
