Rails.application.routes.draw do
  post "/login", to: 'authentication#login'
  post "/sign_up_user", to: 'authentication#sign_up_user'

  post "/close_loan/:id", to: "loans#close_loan"
  post "/create_loan", to: "loans#create"

  post "/add_book", to: "books#create"

  post "/add_author", to: "authors#create"

  resources :loans
  resources :users
  resources :books
  resources :authors
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
