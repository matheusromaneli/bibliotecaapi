class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :name
      t.string :url_image
      t.integer :edition
      t.string :theme
      t.integer :issue
      t.string :kind
      t.references :author, null: false, foreign_key: true

      t.timestamps
    end
  end
end
