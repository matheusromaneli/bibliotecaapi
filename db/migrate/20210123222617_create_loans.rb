class CreateLoans < ActiveRecord::Migration[6.1]
  def change
    create_table :loans do |t|
      t.string :status
      t.references :user, null: false, foreign_key: true
      t.references :book, null: false, foreign_key: true
      t.float :fine
      t.date :delay_days
      t.date :expiry_date

      t.timestamps
    end
  end
end
